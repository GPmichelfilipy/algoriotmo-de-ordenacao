import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
		
		// gerando vetor desordenado apartir do m�todo construtor
		System.out.println("Imprimindo 10 n�meros de 0 a 50 aleatoriamente:");
		int[] vetordesordenado = gerarVetor(10);
		
				//Menu Principal
		
			System.out.println(Arrays.toString(vetordesordenado));
			System.out.println("\n------------------------MENU PRINCIPAL------------------------");
			System.out.println("Escolha a op��o referente ao algoritmo de Ordena��o desejado: ");
			System.out.println("=                        1- Bubble Sort                      =");
			System.out.println("=                        2- Selection Sort                   =");
			System.out.println("=                        3- Insertion Sort                   =");
			System.out.println("=                        4- Merge Sort                       =");
			System.out.println("=                        5- Quick Sort                       =");
			System.out.println("--------------------------------------------------------------");
			System.out.println("Digite o n�mero correspondente ao tipo de Ordena��o: ");
			
			Scanner entrada = new Scanner(System.in);
			int tipo = entrada.nextInt();
			
			if (tipo == 1) {
				System.out.println("Ordenando com Bubble Sort: ");
				bubbleSort(vetordesordenado);
				System.out.println(Arrays.toString(vetordesordenado));
			}
			else if (tipo == 2) {
				System.out.println("Ordenando com Selection Sort: ");
				selectionSort(vetordesordenado);
				System.out.println(Arrays.toString(vetordesordenado));
			}
			else if (tipo == 3) {
				System.out.println("Ordenando com Insertion Sort: ");
				insertionSort(vetordesordenado);
				System.out.println(Arrays.toString(vetordesordenado));
			}
			else if (tipo == 4) {
				System.out.println("Ordenando com Merge Sort: ");
				mergeSort(vetordesordenado, new int[vetordesordenado.length], 0, vetordesordenado.length-1);
				System.out.println(Arrays.toString(vetordesordenado));
			}
			else if (tipo == 5) {
				System.out.println("Ordenando com Quick Sort: ");
				quickSort(vetordesordenado, 0, vetordesordenado.length-1);
				System.out.println(Arrays.toString(vetordesordenado));
			}
			else {
				System.out.println("Ordenado!!!");
			}
			
	}
	
	// ---------------GERADOR DE N�MEROS ALEAT�RIOS
	private static int[] gerarVetor(int tam) {
		int []vetor = new int[tam];
		Random gerador = new Random();
		
		for(int i =0; i < vetor.length; i++) {
			vetor[i] = gerador.nextInt(50);
		}
		return vetor;
	}
	
	//-------------------M�TODO QUICK SORT
	//divisao e conquista
	// escolhe um pivo e ordena de forma que todos os elementos anteriores sejam menores e posteriores maiores 
	// rearranja as chaves de maneira que as maiores precedam as menores
	//ordena as sublistas de chaves menores e maiores recursivamente at� que esteja ordenada
	private static int[] quickSort(int[] v, int esq, int dir) {
		if(esq < dir) {
			int j = separar(v, esq, dir);
			quickSort(v, esq, j-1);
			quickSort(v, j+1, dir);
		}
		return v;
	}


	private static int separar(int[] v, int esq, int dir) {
		int i =esq+1;
		int j = dir;
		int pivo = v[esq];
		while(i<= j) {
			if (v[i] <= pivo)
				i++;
			else if (v[j] > pivo)
				j--;
			else if(i<=j) {
				trocarQ(v,i,j);
				i++;
				j--;
			}
		}
		trocarQ(v, esq, j);
		return j;
	}


	private static void trocarQ(int[] v, int i, int j) {
		int aux = v [i];
		v[i] = v[j];
		v[j] = aux;
	}

	// ---------------METODO MERGE SORT
	// divis�o e conquista - alto consumo de memoria
	//divide em subproblemas - resolve atrav�s da recursividade -apos a resolu�ao ocorre a conquista que � a uniao dos subproblemas
	private static int[] mergeSort(int[] v, int[] z, int inicio, int fim) {
		if(inicio < fim) {
			int meio = (inicio+fim)/2;
			mergeSort(v, z, inicio, meio);
			mergeSort(v,z,meio+1, fim);
			intercalar(v, z, inicio, meio, fim);
		}
		return v;
	}


	private static void intercalar(int[] v, int[] z, int inicio, int meio, int fim) {
		for (int k = inicio; k <= fim; k++)
			z[k] = v[k];
		int i = inicio;
		int j = meio + 1;
		
		for(int k=inicio; k <= fim; k++) {
			if (i > meio) v[k] = z[j++];
			else if (j > fim) v[k] = z[i++];
			else if (z[i] < z[j]) v[k] = z[i++];
			else v[k] = z[j++];
		}
		
	}

	//------------------METODO INSERTION SORT
	//controi uma lista final e vai adicionando os elementos por vez
	private static int[] insertionSort(int[] vetor) {
		int aux, j;
		for(int i=1; i < vetor.length; i++) {
			aux = vetor[i];
			j = i-1;
			
			while((j>=0) && (vetor[j]> aux)) {
				vetor[j+1] = vetor[j];
				j--;
			}
			vetor[j+1] = aux;
		}
		return vetor;
	}

	//----------------METODO SELECTION SORT
	         // sempre passa o menor valor para a primeira posi�ao o segundo menor para a segunda posi�ao 
	            // e assim � feito com os n-1 elementos restantes
	private static int[] selectionSort(int[] vetor) {
		for (int i = 0; i < vetor.length; i++) {
			int menor = i;
			for (int j = i+1; j < vetor.length;j++) {
				if (vetor[j] < vetor[menor])
					menor = j;
			}
				trocarB(vetor, i , menor);	
		}
		return vetor;
	}

	private static void trocarB(int[] vetor, int i, int menor) {
		int aux = vetor[i];
		vetor[i] = vetor[menor];
		vetor[menor] = aux;
	}

	// ---------METODO BUBBLE SORT      Percorre o vetor diversas vezes e a cada passagem faz flutuar
	                                // o maior valor para o final da lista e 
	private static int[] bubbleSort(int[] vetor) {
		   int n = vetor.length;
		    
		    for ( int i=0; i<n; i++) {
		        for ( int j=0; j<n-1; j++) {
		            if (vetor[j] > vetor[j+1]) {
		                int aux = vetor[j];
		                vetor[j] = vetor[j+1];
		                vetor[j+1] = aux;
		            }
		        }
		    }
		    return vetor;
		
	}
}
